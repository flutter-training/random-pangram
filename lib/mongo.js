const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://th:th@mongoatlas-skevj.gcp.mongodb.net/test?retryWrites=true&w=majority";
const dbName = 'pangram';
const collName = 'view_pangram';



const client = new MongoClient(uri, {
    useNewUrlParser: true,
    haInterval: 500,
    useUnifiedTopology: true
});

exports.startMongo = async () => {
    await checkDBConnection();
};

exports.getRandomPangram = async () => {
    const newCL = await checkDBConnection();
    return await newCL.db(dbName).collection(collName)
        .aggregate([{ $sample: { size: 1 } }])
        .toArray()
        .then(datas => datas[0])
        .catch((err) => {
            client.close();
            throw err;
        });
};

exports.addNewWord = async (data) => {
    const newCL = await checkDBConnection();
    return await newCL.db('anagram').collection('words1')
        .insertMany(data)
        .then(data => data)
        .catch(err => {
            console.log('data ', data);
            console.log('error ', err);
            throw err;
        });
}
exports.addOneWord = async (data) => {
    const newCL = await checkDBConnection();
    return await newCL.db('anagram').collection('words')
        .updateOne(
            { word: data.word },
            { $set: data },
            { upsert: true },
        ).then(data => data);
}

exports.aggregate = async (aggregates) => {
    const newCL = await checkDBConnection();
    return await newCL.db('anagram').collection('words1')
        .aggregate(aggregates).toArray().then(data => data);
}

exports.helloAsync = async (name) => {
    return new Promise(resolve => resolve('Hello ' + name));
};


const checkDBConnection = async () => {
    if (!client.isConnected()) {
        await client.connect().catch(err => {
            console.log('error while checking DB connection: ');
            throw err;
        });
    }
    return client;
};