const axios = require('axios').default;

exports.get = async (uri, headers) => {
    return axios.get(uri, {
        headers: headers,
    });
}

exports.getStream = async (uri, headers) => {
    return axios.get(uri, {
        headers: headers,
        responseType: 'stream'
    }).then(val => val.data).catch(err => console.log(err));
}