'use strict';

const { getRandomPangram } = require('../mongo');

const internal = {}

internal.pangramConfig = {
    handler: (response, h) => pangramHandler(response, h),
    description: 'Get Random Pangram',
    notes: 'Returns Random Pangram',
    tags: ['api'],
};

const pangramHandler = async (response, h) => {
    return await getRandomPangram()
        .catch(err => {
            throw err;
        });
}

module.exports = internal;