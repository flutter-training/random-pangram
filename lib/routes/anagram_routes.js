'use strict';

const handlers = require('../handlers');

const internal = {};

internal.routes = [
    {
        method: 'GET',
        path: '/anagram',
        options: handlers.AnagramHandler.anagramConfig,
    },
    {
        method: 'GET',
        path: '/anagram/all',
        options: handlers.AnagramHandler.anagramAllConfig,
    },
    {
        method: 'POST',
        path: '/anagram/word',
        options: handlers.AnagramHandler.anagramWordConfig,
    },
];

module.exports = internal.routes;