'use strict';

const network = require('../core/network/network');
const readline = require('readline');
const mongo = require('../mongo');

const internals = {}

internals.isValidWord = async (word) => {
    return !((await network.get(`http://www.anagramica.com/lookup/${word}`)).data.found === 0);
}

internals.getLines = async (stream) => {
    const lineStream = readline.createInterface({
        input: stream,
        console: false,
    });

    const anagrams = [];

    lineStream.on('line', async (str) => {
        const isValid = true;

        if (isValid) {
            const word = {
                'word': str,
                'sorted': internals.sort(str),
            };
            // mongo.addOneWord(word);
            anagrams.push(word);
        }
    });

    lineStream.on('close', async () => {
        console.log('insert data to mongo');
        await mongo.addNewWord(anagrams);
        console.log('data successfully inserted');
    });

}

internals.sort = (str) => {
    return [...str.toLowerCase()]
        .sort(ascendingSort)
        .reduce((a, b) => a + b);
}

const ascendingSort = (a, b) => a.charCodeAt(0) - b.charCodeAt(0);

module.exports = internals;