'use strict';

const Joi = require('@hapi/joi');
const Services = require('../services');
const network = require('../core/network/network');
const mongo = require('../mongo');

const internals = {};

internals.anagramConfig = {
    description: 'Check Anagram',
    notes: 'Returns Anagram',
    tags: ['api'],
    handler: (request, h) => anagramHandler(request, h),
    validate: {
        query: Joi.object(
            {
                word1: Joi.string().min(1).required(),
                word2: Joi.string().min(1).required(),
            }
        ),
    },
}

internals.anagramAllConfig = {
    description: 'Check Anagram',
    notes: 'Returns Anagram',
    tags: ['api'],
    handler: (request, h) => anagramAllHandler(request, h),
}

internals.anagramWordConfig = {
    description: 'Add Anagram Word',
    notes: 'add word to anagram dictionary',
    tags: ['api'],
    handler: (request, h) => anagramWordHandler(request, h),
    validate: {
        payload: Joi.object(
            {
                url: Joi.string().min(1).required(),
            }
        ),
    },
}


const anagramWordHandler = async (request, h) => {
    const stream = await network.getStream(request.payload.url);
    return await Services.WordService
        .getLines(stream)
        .then(() => 'Done, data inserted!')
        .catch((err) => { throw err; });
}

const anagramAllHandler = async (request, h) => {
    const aggregate = [{
        $group: {
            _id: '$sorted',
            'sum': {
                $sum: 1
            },
            'anagrams': {
                $addToSet: '$word'
            }
        }
    }, {
        $match: {
            'sum': {
                $gt: 1
            }
        }
    }];
    const datas = await mongo.aggregate(aggregate);
    return datas;
}

const anagramHandler = async (request, h) => {
    const isValid =
        (await Services.WordService.isValidWord(request.query.word1)) &&
        (await Services.WordService.isValidWord(request.query.word2));
    return isValid ?
        Services.AnagramService.isAnagram(request.query.word1, request.query.word2) :
        false;
}

module.exports = internals;