'use strict';

const handlers = require('../handlers');

const internal = {};

internal.routes = [
    {
        method: 'GET',
        path: '/pangram',
        options: handlers.PangramHandler.pangramConfig,
    }
];

module.exports = internal.routes;